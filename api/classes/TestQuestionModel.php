<?php

class TestQuestionModel extends Model
{
    protected $table = "testQuestions";
    protected $cols = ["question"];

	function getData($request, $requireID = false)
	{
		$reqData = $request->getParsedBody();
		$data = [];
		$requiredFields = ["question"];
		$data["id"] = !empty($reqData["id"]) ? filter_var($reqData["id"], FILTER_SANITIZE_NUMBER_INT) : "";
		$data["question"] = filter_var($reqData["question"], FILTER_SANITIZE_STRING);

		return $this->getDataResponse($data, $requiredFields, $requireID);
	}
}
