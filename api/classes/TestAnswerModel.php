<?php

class TestAnswerModel extends Model
{
    protected $table = "testAnswers";
    protected $cols = ["answer", "correct", 'questionID'];

	function getData($request, $requireID = false) {
		$reqData = $request->getParsedBody();
		$data = [];
		$requiredFields = ["answer", "correct"];
		$data["id"] = !empty($reqData["id"]) ?  filter_var($reqData["id"], FILTER_SANITIZE_NUMBER_INT) : "";
		$data["answer"] = filter_var($reqData["answer"], FILTER_SANITIZE_STRING);
		$data["correct"] = !empty($reqData["correct"]) ?  filter_var($reqData["correct"], FILTER_SANITIZE_NUMBER_INT) : "";
		$data["questionID"] = !empty($reqData["correct"]) ?  filter_var($reqData["questionID"], FILTER_SANITIZE_NUMBER_INT) : "";

		return $this->getDataResponse($data, $requiredFields, $requireID);
	}
}