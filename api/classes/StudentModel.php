<?php


class StudentModel extends Model
{
    protected $table = "students";
    protected $cols = ["firstName", "lastName", 'idCode'];

    function getData($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ["firstName", "lastName", 'idCode'];
        $data["id"] = !empty($reqData["id"]) ?  filter_var($reqData["id"], FILTER_SANITIZE_NUMBER_INT) : "";
        $data["firstName"] = filter_var($reqData["firstName"], FILTER_SANITIZE_STRING);
        $data["lastName"] = filter_var($reqData["lastName"], FILTER_SANITIZE_STRING);
        $data["idCode"] = !empty($reqData["idCode"]) ?  filter_var($reqData["idCode"], FILTER_SANITIZE_NUMBER_INT) : "";

        return $this->getDataResponse($data, $requiredFields, $requireID);
    }
}