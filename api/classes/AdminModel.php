<?php


class AdminModel extends Model
{
    protected $table = 'admins';
    protected $cols = ['email', 'firstName', 'lastName'];

    public function getUserByEmail($email) {
        $user = $this->filter('email', $email, ['id']);
        return empty($user) ? '' : $user['id'];
    }

    public function getOrCreateAdmin(array $data){
        $result = $this->getUserByEmail($data["email"]);
        return empty($result) ? $this->newRow($data, true) : $result;
    }

    public function getOrUpdateAdmin(array $data){
        $result = $this->getUserByEmail($data["email"]);
        if (empty($result)) {
            http_response_code(401);
            exit(json_encode(['error' => 'Teie Google kontol: '.$data['email'].' ei ole administraatori õiguseid!' ]));
        }
        $data['id'] = $result;
        $this->updateRow($data, true);

        return $result;
    }


    function getData($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ["email"];
        $data["email"] = filter_var($reqData["email"], FILTER_SANITIZE_EMAIL);
        $data["firstName"] = !empty($reqData["firstName"]) ?  filter_var($reqData["firstName"], FILTER_SANITIZE_STRING) : "";
        $data["lastName"] = !empty($reqData["lastName"]) ?  filter_var($reqData["lastName"], FILTER_SANITIZE_STRING) : "";

        return $this->getDataResponse($data, $requiredFields, $requireID);
    }

    function getAll(array $cols = []) {
        $cols = empty($cols) ? $this->cols : $cols;
        return $this->filter('email','test@test.ee', $cols, '!=');

    }

    function remove($id)
    {
        $statement = $this->db->delete()->from($this->table)->where('id', '=', $id)->where('email', '!=', 'test@test.ee');
        $result = $statement->execute();

        if ($result) {
            http_response_code(200);
            exit(json_encode(['message' => 'OK']));
        }
        http_response_code(404);
        exit(json_encode(['message' => 'Not found']));
    }
}