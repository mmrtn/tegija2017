<?php


class TestLogModel extends Model
{
    protected $table = "testLog";
    protected $cols = ["answerID", "studentID"];

    function getData($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ["answerID"];
        $data["answerID"] = !empty($reqData["answerID"]) ?  filter_var($reqData["answerID"],
            FILTER_SANITIZE_NUMBER_INT) : "";

        return $this->getDataResponse($data, $requiredFields, $requireID);
    }

    function newAnswer(array $data) {
        $this->db->insert($data);
    }

    function getTestResult($studentID) {
        $statement = $this->db->select([
            'sum(testAnswers.correct) as correctCount',
            'sum(1) as answersCount'

        ])->from($this->table)->leftJoin('testAnswers', 'testLog.answerID = testAnswers.id')->where('studentID', '=',
                $studentID)->groupBy('studentID');

        return $statement->execute()->fetchAll();
    }

    function getSudentResult($studentID) {
        $statement = $this->db->select([
            'testLog.answerID AS student_answer',
            'testAnswers.correct'
        ])->from($this->table)->leftJoin('testAnswers', 'testLog.answerID = testAnswers.id')->where('studentID', '=', $studentID);

        return $statement->execute()->fetchAll();
    }

    function getAllResults() {
        $statement = $this->db->select([
            'studentID',
            'sum(correct) AS correctCount',
            'sum(1) as answersCount'
        ])
            ->from($this->table)->leftJoin('testAnswers', 'testLog.answerID = testAnswers.id')
            ->groupBy('studentID')
            ->orderBy('correctCount');

        return $statement->execute()->fetchAll();
    }
}