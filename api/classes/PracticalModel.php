<?php


class PracticalModel extends Model
{
    protected $table = 'practicalQuestions';
    protected $cols = ['question', 'imgUrl'];

    function getQuiz(){
        $statement = $this->db->select($this->getCols(true))->from($this->table)->orderBy('RAND()')
            ->limit(1, 0);
        $result = $statement->execute()->fetch();

        return $result;
    }

    function getData($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ["question"];
        $data["id"] = !empty($reqData["id"]) ?  filter_var($reqData["id"], FILTER_SANITIZE_NUMBER_INT) : "";
        $data["question"] = filter_var($reqData["question"], FILTER_SANITIZE_STRING);
        $data["imgUrl"] = filter_var($reqData["imgUrl"], FILTER_SANITIZE_URL);

        return $this->getDataResponse($data, $requiredFields, $requireID);
    }
}