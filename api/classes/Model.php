<?php

/**
 * Class Mapper
 */

use Slim\PDO\Database;

abstract class Model {

	protected $db;
	protected $table;
	protected $cols;

	/**
	 * Mapper constructor.
	 * @param Database $db
	 */
	public function __construct(Database $db) {
		$this->db = $db;
	}


	/**
	 * @param string $col
	 * @param $val
	 * @param array $cols
	 * @return mixed
	 */
	function filter($col, $val, $cols = [], $condition = '=') {
		$cols = empty($cols) ? $this->getCols(true) : $cols;
		$result = $this->db->select($cols)->where($col, $condition, $val)->from($this->table)->execute()->fetchAll();
		return count($result) == 1 ? $result[0] : $result;
	}

	/**
	 * @param $id
	 * @param array $cols
	 * @return array
	 */
	function getOne($id, $cols = []) {
		$result = $this->filter("id", $id, $cols);

        if ($result) {
            return $result;
        }

        http_response_code(404);
        exit(json_encode(['message' => "Can't find item with id: ". $id.' from table '.$this->table]));;


	}

	/**
	 * @param array $cols
	 * @return mixed
	 * @internal param $statement
	 */
	function getAll(array $cols = []){
		$cols = empty($cols) ? $this->getCols(true) : $cols;
		return $this->db->select($cols)->from($this->table)->execute()->fetchAll();
	}

	/**
	 * @param array $data
	 * @return string
	 * @throws Exception
	 */
	function newRow(array $data, $continue = False, $cols = []){
		$values = [];
        $cols = empty($cols) ? $this->getCols(false) : $cols;

		foreach ($cols as $value){
            if(!empty($data[$value] or $data[$value] === 0)){
                $values [$value]= $data[$value];
            }
		}

		$statement = $this->db->insert($values)->into($this->table);
		$result = $statement->execute();
        if ($continue) {
            return $result;
        }

        http_response_code(201);
        exit(json_encode(['message' => "Created"]));
	}

	/**
	 * @param array $data
	 * @return integer
	 * @throws Exception
	 */
	function updateRow(array $data, $continue = False){
		$values = [];

        $cols = $this->getCols(true);
		foreach ($cols as $value){
			if(!empty($data[$value]) or $data[$value] === 0){
				$values [$value]= $data[$value];
			}
		}
		$statement = $this->db->update($values)->table($this->table)->where("id", "=", $data["id"]);
		$result = $statement->execute();

        if ($result == 1) {
            if (!$continue) {
                http_response_code(200);
                exit(json_encode(['message' => 'OK']));
            }
        }

        if ($this->getOne($data["id"])) {
            if (!$continue) {
                http_response_code(304);
                exit(json_encode(["message" => "Not updated, because same data"]));
            }
        }

        return $result;

	}

	/**
	 * @param $id
	 * @return array
	 */
	function remove($id){
		$statement = $this->db->delete()->from($this->table)->where('id', '=', $id);
		$result = $statement->execute();

		if ($result) {
            http_response_code(200);
            exit(json_encode(['message' => 'OK']));
		}
        http_response_code(404);
        exit(json_encode(['message' => 'Not found']));
	}

	function getDataResponse($data, $requiredFields, $requireID){
		if ($requireID) {
			$requiredFields []= "id";
		}
		$missingFields = [];

		foreach ($requiredFields as $required_field){
			if (!array_key_exists($required_field, $data) or empty($data[$required_field])) {
				$missingFields []= $required_field;
			}
		}
		if (!empty($missingFields)) {
            http_response_code(422);
            exit(json_encode(['invalidFields' => $missingFields]));
		}


		return $data;
	}

	function getCols($withID=True) {
	    return $withID ? array_merge($this->cols, ['id']) : array_values(array_diff($this->cols, ["id"]));
    }
}
