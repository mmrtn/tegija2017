<?php


use Slim\PDO\Database;

class SessionModel extends Model
{

    protected $table = "sessions";
    protected $idColumn = 'studentID';
    protected $mode = 'student';
    protected $tokenLenght = 10;
    protected $prefix = '';

    public function __construct(Database $db, $mode = '')
    {
        if ($mode == 'admin') {
            $this->idColumn = 'adminID';
            $this->tokenLenght = 11;
            $this->mode = $mode;
            $this->prefix = 'ADMIN';
        }
        parent::__construct($db);
    }

    public function validateSession($token) {
        return  $this->filter("token", $token, [$this->idColumn])[$this->idColumn];
    }

    function newSession($userID) {
        $data = [
            "token" => $this->prefix.hash('sha256', mt_rand()),
            $this->idColumn => $userID
        ];
        $cols=["token", $this->idColumn];
        $this->newRow($data, true, $cols);
        return $data["token"];
    }

    public function endSession($token){
        $statement = $this->db->delete()->from($this->table)->where("token", "=", $token);
        $affected = $statement->execute();
        if (!$affected){
            throw new Exception("Nothing changed");
        }
    }

}