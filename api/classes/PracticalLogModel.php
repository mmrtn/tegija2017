<?php


class PracticalLogModel extends Model
{
    protected $table = 'practicalLog';
    protected $cols = ['answer', 'questionID', 'points'];


    function validateStudent($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ["questionID", "answer"];
        $data["questionID"] = !empty($reqData["questionID"]) ?  filter_var($reqData["questionID"],
            FILTER_SANITIZE_NUMBER_INT) : "";
        $data["answer"] = $reqData["answer"];
        return $this->getDataResponse($data, $requiredFields, $requireID);
    }

    function validateAdmin($request, $requireID = false) {
        $reqData = $request->getParsedBody();
        $data = [];
        $requiredFields = ['id', 'points'];
        $data["id"] = !empty($reqData["id"]) ?  filter_var($reqData["id"],
            FILTER_SANITIZE_NUMBER_INT) : "";
        $data["points"] = !empty($reqData["points"]) ?  filter_var($reqData["points"],
            FILTER_SANITIZE_NUMBER_FLOAT) : "";
        return $this->getDataResponse($data, $requiredFields, $requireID);
    }

    function getResults() {
        $statement = $this->db->select([
            'SUM(points) as points',
            'studentID',
            'firstName',
            'lastName',
            'idCode'
        ])->from($this->table)->groupBy('studentID')->orderBy('points')->leftJoin('students', 'students.id', '=', 'studentID');

        $result = $statement->execute()->fetchAll();

        return $result;
    }

    function getQuestionAnswers($question_id) {
        return $this->filter('questionID', $question_id, ['id', 'answer', 'points']);
    }


    function getAnswer($answerID) {
        $statement = $this->db->select(['answer', 'firstName', 'lastName'])->from($this->table)->leftJoin('students', 'students.id', '=', 'studentID')->where($this->table.'.id', '=',$answerID);
        $result = $statement->execute()->fetch();
        return $result;
    }
}