<?php


class QuizModel extends Model
{

    function getRandomQuestions() {
        return $this->getQuiz(false);
    }

    function getOne($id, $cols = [], $create_messgae = false) {
        return $this->getQuiz(true, $id);
    }

    function getQuiz($admin=true, $questionID=0){
        $randAndlimit = $admin ? '' :  'ORDER BY RAND() LIMIT 10';
        $whereQuestionID = $questionID === 0 ? '' : "WHERE id = $questionID" ;

        $cols = [
            "questionID",
            "question",
            "answer",
            "testAnswers.id AS answerID"
        ];

        if ($admin) {
            $cols []= "correct";
        }

        $statement = $this->db->select($cols
        )->from("(
            SELECT
                id,
                question 
             FROM testQuestions 
             $whereQuestionID
             $randAndlimit) questions"
        )->leftJoin('testAnswers', 'testAnswers.questionID', '=', 'questions.id');;

        // Student guiz answers is in random order
        $statement = $admin ? $statement : $statement->orderBy('RAND()');

        $raw = $statement->execute()->fetchAll();

        $result = [];

        foreach ($raw as $value) {
            $key = $value['questionID'];
            $result[$key]['question'] = $value['question'];
            $result[$key]['questionID'] = $value['questionID'];
            $anwser = ['answer' => $value['answer'], 'answerID' => $value['answerID']];
            $anwser['correct'] = $admin ? $value['correct'] : 0;
            $result[$key]['answers'] []= $anwser;
        }

        return array_values($result);
    }
}