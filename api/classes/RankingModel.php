<?php


class RankingModel extends Model
{
    function getAll(array $cols = []) {
        $statement = $this->db->select([
           'students.firstName',
           'students.lastName',
           'students.idCode',
           'max(x.correct)                 AS test',
           'max(x.points)                  AS practical',
           'sum(x.correct) + sum(x.points) AS total',
            'COALESCE((SELECT id from practicalLog WHERE studentID = x.studentID ORDER BY created DESC LIMIT 1),0) AS lastPracticalAnswerID',
            'students.created as started',
            'MAX(x.ended) as ended'
        ])->from("
                (
               SELECT
                 0           AS correct,
                 sum(points) AS points,
                 MAX(created) as ended,
                 studentID
               FROM practicalLog
               GROUP BY studentID

               UNION
               SELECT
                 sum(correct) AS correct,
                 0            AS points,
                 ''      AS ended,
              studentID
               FROM testLog
                 INNER JOIN testAnswers ON testAnswers.id
                                            = testLog.answerID
               WHERE correct = 1
               GROUP BY
                 studentID
             ) x
     ")->leftJoin('students', 'students.id', '=', 'x.studentID')->GroupBy('studentID')->orderBy('total', 'DESC');


        $result = $statement->execute()->fetchAll();

        // Quiock hack for fix row with empty name.
        $data = [];
        foreach ($result as $r) {
            if (!empty($r['firstName'])) {
                $data[]=$r;
            }
        }
        return $data;
    }
}