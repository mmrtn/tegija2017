<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

spl_autoload_register(function ($classname) {
    require(__DIR__ . "/../classes/" . $classname . ".php");
});



function getHeaderToken($request){
    $key = $request->getHeader("X-Token");
    if ($key) {
        return filter_var($key[0], FILTER_SANITIZE_STRING);
    };
    http_response_code(401);
    exit(json_encode(['error' => "No X-Token in header"]));
}

function loadSession(Request $request, $db, $mode = '')
{
    $data["sessionKey"] = getHeaderToken($request);
    $mapper = new SessionModel($db, $mode);
    $user = $mapper->validateSession($data["sessionKey"]);
    if ($user === False or $user == null) {
        http_response_code(401);
        exit(json_encode(['error' => "Invalid token"]));
    }
    return $user;
}

/**
 * @api {post} /student Create new student
 * @apiName Student
 * @apiGroup Student
 *
 * @apiParam {String} firstName Firstname of the student.
 * @apiParam {String} lastName  Lastname of the student.
 * @apiParam {String} idCode  Idcode of the student.
 *
 * @apiSuccess {String} token Session token.
 */
$app->post("/student", function (Request $request, Response $response, $args) {
    $student = new StudentModel($this->db);
    $session  = new SessionModel($this->db);
    $data = $student->getData($request, false);

    if (array_key_exists(0, $data)){
        return $response->withJSON(["invalidFields" => $data], 422);
    }

    if (FALSE && $student->filter('idcode', $data['idCode'], ['idCode'])) {
        return $response->withJSON(["message" => 'Found student with this idcode: '.$data['idCode']]);
    };
    $studentID = $student->newRow($data, true);
    $token = $session->newSession($studentID);



    $response = $response->withJSON(["token" => $token]);
    return $response;
});

$app->group('/headers', function () use ($app) {

    $app->get("", function (Request $request, Response $response, $args) {
        $response = $response->withJSON($request->getHeaders("*"));
        return $response;
    });

    $app->post("", function (Request $request, Response $response, $args) {
        $response = $response->withJSON($request->getHeaders("*"));
        return $response;
    });

    $app->put("", function (Request $request, Response $response, $args) {
        $response = $response->withJSON($request->getHeaders("*"));
        return $response;
    });

    $app->delete("", function (Request $request, Response $response, $args) {
        $response = $response->withJSON($request->getHeaders("*"));
        return $response;
    });
});

$app->group('/test', function () use ($app) {

    /**
     * @api {get} /test Get 10 questions for quiz
     * @apiName Test
     * @apiGroup Test
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *  [
     *      {
     *              "question": "Lingitud pildi loomiseks kasutatakse..",
     *              "questionID": 20,
     *              "answers": [{
     *                      "answer": "img tag-i a tagi sees",
     *                      "answerID": 60,
     *                      "correct": 0
     *              },
     *                      {
     *                              "answer": "img tag-i",
     *                              "answerID": 59,
     *                              "correct": 0
     *                      },
     *                      {
     *                              "answer": "a tag-i",
     *                              "answerID": 58,
     *                              "correct": 0
     *                      }
     *              ]
     *      },
     *      {
     *              "question": "Milline on väikseim arv tag'e, millega saaks teha kõige lihsama, aga korrektse veebilehe?",
     *              "questionID": 26,
     *              "answers": [{
     *                      "answer": "10 kokku (5paari)",
     *                      "answerID": 77,
     *                      "correct": 0
     *              },
     *                      {
     *                              "answer": "8 kokku (4 paari)",
     *                              "answerID": 76,
     *                              "correct": 0
     *                      },
     *                      {
     *                              "answer": "4 kokku (2 paari)",
     *                              "answerID": 78,
     *                              "correct": 0
     *                      }
     *              ]
     *      }
     *  ]
     */
    $app->get("", function (Request $request, Response $response, $args) {
        $studentID = loadSession($request, $this->db, 'student');
        $questions = new QuizModel($this->db);
        $game = $questions->getRandomQuestions();


        $response = $response->withJSON($game);
        return $response;
    });

    $app->post("", function (Request $request, Response $response, $args) {
        $studentID = loadSession($request, $this->db, 'student');

        $testlog = new TestLogModel($this->db);
        $data = $testlog->getData($request, false);

        if (array_key_exists(0, $data)){
            return $response->withJSON(["invalidFields" => $data], 422);
        }


        $data['studentID'] = $studentID;
        $res = $testlog->newRow($data);

        if ($res) {
            return $response->withJSON(["message" => "Created"], 201);
        };

        return $response->withJSON($res);
    });

    $app->get("/results", function (Request $request, Response $response, $args) {
        $studentID = loadSession($request, $this->db, 'student');

        $testlog = new TestLogModel($this->db);
        $result = $testlog->getTestResult($studentID);

        $response = $response->withJSON($result);
        return $response;
    });
});


$app->group('/practical', function () use ($app) {
    $app->get("", function (Request $request, Response $response, $args) {
        $studentID = loadSession($request, $this->db, 'student');

        $practical = new PracticalModel($this->db);
        $result = $practical->getQuiz();

        $response = $response->withJSON($result);
        return $response;

    });

    $app->post("", function (Request $request, Response $response, $args) {
        $studentID = loadSession($request, $this->db, 'student');

        $practical = new PracticalLogModel($this->db);

        $data = $practical->validateStudent($request);
        if (array_key_exists(0, $data)){
            return $response->withJSON(["invalidFields" => $data], 422);
        }

        $data['studentID'] = $studentID;
        $result = $practical->newRow($data, false, ['answer', 'questionID', 'studentID']);

        return $response->withJSON($result);
    });

});


$app->group('/admin', function () use ($app) {
    function setClient($code)
    {
        $client = new Google_Client();
        $client->setAuthConfigFile('../client_secrets.json');
        $client->setRedirectUri("postmessage");
        $client->setAccessType('online');
        $client->authenticate($code);
        return $client;
    }

    $app->group('/admins', function () use ($app) {
        $app->get("", function (Request $request, Response $response, $args) {
            $adminID = loadSession($request, $this->db, 'admin');
            $admins = new AdminModel($this->db);
            $result = $admins->getAll();

            $response = $response->withJSON($result);
            return $response;
        });

        $app->post("", function (Request $request, Response $response, $args) {
            $adminID = loadSession($request, $this->db, 'admin');
            $admins = new AdminModel($this->db);
            $data = $admins->getData($request);
            $admins->newRow($data);

        });

        $app->delete("/{id}", function (Request $request, Response $response, $args) {
            $adminID = loadSession($request, $this->db, 'admin');
            if ($args["id"] == $adminID) {
                return $response->withJSON(['error'=>"Can't delete yourself!"], 401);
            }
            $admins = new AdminModel($this->db);
            $admins->remove($args["id"]);
        });

    });

    $app->get("/resetresults", function (Request $request, Response $response, $args) {
        $adminID = loadSession($request, $this->db, 'admin');
        $practical = new PracticalLogModel($this->db);
        $test = new TestLogModel($this->db);
        $test->remove('*');
        $practical->remove('*');
    });

    $app->post("/auth", function (Request $request, Response $response, $args) {
        $admin = new AdminModel($this->db);
        $data = $request->getParsedBody();
        if ($data['username'] == 'html' && $data['password'] == 'VS15tegijad') {
            $data['email'] = 'test@test.ee';
            $data['firstName'] = 'Test';
            $data['lastName'] = 'Admin';
            $adminID = $admin->getOrCreateAdmin($data);

            $session = new SessionModel($this->db, 'admin');
            $token = $session->newSession($adminID);

            $response = $response->withJSON(['token' => $token]);
            return $response;
        } else {
            $response = $response->withJSON(['error' => "Wrong password"], 401);
            return $response;
        }
    });

    $app->post("/oauth", function (Request $request, Response $response, $args) {
        $req_data = $request->getParsedBody();
        $data = [];
        $data["code"] = filter_var($req_data["code"], FILTER_SANITIZE_STRING);


        $client = setClient($data["code"]);
        $google_oauth = new Google_Service_Oauth2($client);

        $userdata = [
            "email" => $google_oauth->userinfo->get()->email,
            "firstName" => $google_oauth->userinfo->get()->givenName,
            "lastName" => $google_oauth->userinfo->get()->familyName
        ];

        $sessions = new SessionModel($this->db, 'admin');
        $admn = new AdminModel($this->db);
        $admin_id = $admn->getOrUpdateAdmin($userdata);
        if (!$admin_id) {
            return $response->withJSON([
                "message" => "Oauth failed",
            ], 401);
        }
        $token = $sessions->newSession($admin_id);

        $response = $response->withJSON(['token' => $token]);
        return $response;

    });

    $app->post("/logout", function (Request $request, Response $response, $args) {
        $data["session_key"] = filter_var($request->getHeader("X-Token")[0], FILTER_SANITIZE_STRING);
        $session = new SessionModel($this->db);
        $session->endSession($data["session_key"]);

        // Unset all of the session variables.
        $_SESSION = array();
        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
        $result = array(
            "message" => "OK"
        );

        $response = $response->withJSON($result);
        return $response;
    });

    $app->get("/ranking", function (Request $request, Response $response, $args) {
        $adminID = loadSession($request, $this->db, 'admin');

        $ranking = new RankingModel($this->db);
        $result = $ranking->getAll();

        $response = $response->withJSON($result);
        return $response;

    });

    $app->group('/practical', function () use ($app) {
        $app->group('/questions', function () use ($app) {
            $app->get("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $practical = new PracticalModel($this->db);
                $game = $practical->getAll();


                $response = $response->withJSON($game);
                return $response;
            });

            $app->post("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $data = $request->getParsedBody();

                $practical = new PracticalModel($this->db);

                $data = $practical->getData($request);

                if (array_key_exists(0, $data)){
                    return $response->withJSON(["invalidFields" => $data], 422);
                }

                $result = $practical->newRow($data);

                return $response->withJSON($result, 200);
            });


            $app->put("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $data = $request->getParsedBody();

                $practical = new PracticalModel($this->db);

                $data = $practical->getData($request, true);

                if (array_key_exists(0, $data)){
                    return $response->withJSON(["invalidFields" => $data], 422);
                }

                $result = $practical->updateRow($data);

                return $response->withJSON($result, 200);
            });

            $app->delete("/{id}", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $practical = new PracticalModel($this->db);
                $result = $practical->remove($args["id"]);

                return $response->withJSON($result[0], $result[1]);
            });
        });


        $app->group('/answers', function () use ($app) {
            $app->get("/{question_id}", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $practicallog = new PracticalLogModel($this->db);
                $result = $practicallog->getQuestionAnswers($args['question_id']);

                $response = $response->withJSON($result);
                return $response;
            });

            $app->get("/view/{answerID}", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $practical = new PracticalLogModel($this->db);
                $answer = $practical->getAnswer($args['answerID']);
                return $this->view->render($response, 'practical.phtml', $answer);
            });

            $app->put("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $practicallog = new PracticalLogModel($this->db);
                $data = $practicallog->validateAdmin($request);

                if (array_key_exists(0, $data)){
                    return $response->withJSON(["invalidFields" => $data], 422);
                }

                $result = $practicallog->updateRow($data);

                $response = $response->withJSON($result);
                return $response;
            });
        });
    });

    $app->group('/test', function () use ($app) {
        $app->group('/quiz', function () use ($app) {
            $app->get("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $quiz = new QuizModel($this->db);
                $game = $quiz->getQuiz();


                $response = $response->withJSON($game);
                return $response;
            });

            $app->post("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $r = $request->getParsedBody();

                $questions = new TestQuestionModel($this->db);
                $answers = new TestAnswerModel($this->db);
                $data = [];
                $data['questionID'] = $questions->newRow($r, True);
                foreach ($r['answers'] as $a) {
                    $a['questionID'] = $data['questionID'];
                    $data['answers'] [] = $answers->newRow($a, True);
                }

                /*
                if (array_key_exists(0, $data)){
                    return $response->withJSON(["invalidFields" => $data], 422);
                }
                */

                $response = $response->withJSON($data);
                return $response;
            });


            $app->put("", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $r = $request->getParsedBody();

                $questions = new TestQuestionModel($this->db);
                $answers = new TestAnswerModel($this->db);
                $r['id'] = $r['questionID'];
                $data['question'] = $questions->updateRow($r, true);
                foreach ($r['answers'] as $a) {
                    $a['questionID'] = $r['questionID'];
                    $a['id'] = $a['answerID'];
                    $data['answers'] [] = $answers->updateRow($a, true);
                }

                if ($data['question'] == 1  or array_sum($data['answers']) > 0) {
                    return $response->withJSON(['message' => "OK"]);
                }

                return $response->withJSON(['message' => 'Not Modified'], 304);

            });

            $app->delete("/{id}", function (Request $request, Response $response, $args) {
                $adminID = loadSession($request, $this->db, 'admin');

                $questions = new TestQuestionModel($this->db);
                $result = $questions->remove($args["id"]);

                return $response->withJSON($result[0], $result[1]);
            });
        });

        $app->get("/results", function (Request $request, Response $response, $args) {
            $adminID = loadSession($request, $this->db, 'admin');

            $log = new TestLogModel($this->db);
            $result = $log->getAllResults();


            $response = $response->withJSON($result);
            return $response;
        });

    });
});

