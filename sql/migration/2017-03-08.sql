ALTER TABLE students CHANGE fiist_name first_name VARCHAR(45) NOT NULL;

CREATE TABLE practical_questions
(
  id INT PRIMARY KEY NOT NULL,
  question MEDIUMTEXT NOT NULL,
  created_at TIMESTAMP
);