CREATE TABLE `practical_questions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `question` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `practical_questions`
  ADD COLUMN `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `question`;


CREATE TABLE `practical_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `answer` MEDIUMTEXT NOT NULL,
  `points` INT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `controlled_by` INT NULL,
  `question_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_practical_log_1_idx` (`controlled_by` ASC),
  INDEX `fk_practical_log_2_idx` (`question_id` ASC),
  CONSTRAINT `fk_practical_log_1`
  FOREIGN KEY (`controlled_by`)
  REFERENCES `tegija`.`admins` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_practical_log_2`
  FOREIGN KEY (`question_id`)
  REFERENCES `practical_questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


ALTER TABLE `practical_log`
  ADD COLUMN `student_id` INT NULL AFTER `question_id`,
  ADD INDEX `fk_practical_log_3_idx` (`student_id` ASC);
ALTER TABLE `practical_log`
  ADD CONSTRAINT `fk_practical_log_3`
FOREIGN KEY (`student_id`)
REFERENCES `students` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

