-- Remove test_ from colum names.
ALTER TABLE test_answers CHANGE test_question_id question_id INT(11) NOT NULL;
ALTER TABLE test_log CHANGE test_question_id question_id INT(11) NOT NULL;
ALTER TABLE test_log CHANGE test_answer_id answer_id INT(11) NOT NULL;