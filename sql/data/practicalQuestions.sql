INSERT INTO practicalQuestions (question, created) VALUES ('HTML dokumendi struktuur
Lisa lehele punane paragrahv “Olen avatud uutele väljakutsetele!”
Lisa eelnevalt loodud paragrahvile tiitel “Väljakutsed, siit ma tulen!”
Lisa lehele pilt suurusega 250x250px aadressilt:  https://s-media-cache-ak0.pinimg.com/originals/1c/bb/54/1cbb542b423fafe951469bcf8651f28c.jpg
Joonda pilt paremale
Muuda pilt lingiks, mis suunab lehele: http://khk.ee/
Lisa kaldkirjas tekst: Eesnimi, Perekonnanimi, Kuupäev', '2017-03-09 10:28:27');
INSERT INTO practicalQuestions (question, created) VALUES ('HTML dokumendi struktuur
Lisa lehele kaldkirjas sinine pealkiri “Minu sisseastumine!”
Lisa link nimega “Tartu Kutsehariduskeskus on parim!” ning mis suunab lehele: http://khk.ee/
Lisa pilt suurusega 250x250px aadressilt: https://www.demilked.com/magazine/wp-content/uploads/2016/09/offensive-cute-greeting-cards-phil-wall-9.jpg
Lisa pildile roheline kast ümber (äärised)
Joonda pilt keskele
Lisa rasvane tekst: Eesnimi, Perekonnanimi, Kuupäev', '2017-03-10 10:13:04');
INSERT INTO practicalQuestions (question, created) VALUES ('HTML dokumendi struktuur
Joonista lehele siniste ääristega kast, mille suurus on 250x250px
Värvi kast seest kollaseks
Lisa lehele link nimega “Ma kardan kooli minna..” ning mis suunab lehele: http://khk.ee/
Muuda lingi värv punaseks
Lisa lehele pilt suurusega 250x250px aadressilt: https://s-media-cache-ak0.pinimg.com/736x/8f/3c/7d/8f3c7dde728bcea55c360bd623563dbe.jpg
Lisa kaldkirjas tekst: Eesnimi, Perekonnanimi, Kuupäev', '2017-03-10 10:13:04');
INSERT INTO practicalQuestions (question, created) VALUES ('HTML dokumendi struktuur
Lisa lehele järjestamata loend liikmetega “Mari”, “Istus”, “Kivil”
Muuda “Mari”, “Istus” ja “Kivil” kirja värv punaseks ning tagataustavärv siniseks
Lisa lehele link nimega “Mulle meeldib kool!” ning mis suunab lehele: http://khk.ee/
Muuda lisatud lingi font Arial’iks
Lisa lehele pilt suurusega 250x250px aadressilt: https://s-media-cache-ak0.pinimg.com/736x/83/2f/02/832f02c5cf28cbdd15ee76d801dd898a.jpg
Lisa rasvane tekst: Eesnimi, Perekonnanimi, Kuupäev', '2017-03-10 10:13:04');
INSERT INTO practicalQuestions (question, created) VALUES ('HTML dokumendi struktuur
Lisa lehele kahe realine tabel, millel mõlemal real on kaks veergu. Esimese rea esimesse veergu kirjuta “Nimi”, teise veergu “Vanus”. Teise rea esimesse veergu kirjuta “Marta”, teise veergu “24”
Määra tabelile ühtlased mustad äärised, muuda esimese rea veergude taustavärv roosaks ja muuda teise rea esimese veeru kirja värv punaseks.
Lisa lehele roheline link nimega “Ko-ko-kool!” ning mis suunab lehele: http://khk.ee/
Lisa lehele pilt suurusega 250x250px aadressilt: https://s-media-cache-ak0.pinimg.com/736x/9f/57/3c/9f573c0e886c44e4f3bcf200bc4486cc.jpg
Joonda pilt keskele
Lisa kaldkirjas tekst: Eesnimi, Perekonnanimi, Kuupäev', '2017-03-10 10:13:04');