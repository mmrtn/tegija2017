INSERT INTO `testQuestions`
    (`id`, `question`) 
VALUES 
(1,'Mis järjestuses kasutatakse margin parameetreid'),
(2,'HTML on ...');

/*!40000 ALTER TABLE `testAnswers` DISABLE KEYS */;
INSERT INTO `testAnswers` 
    (`id`, `answer`, `questionID`, `correct`) 
VALUES 
    (1,'up, right, down, left ',1,1),
    (2,'down, right, up, left',1,0),
    (3,'left, down, right, up',1,0),
    (4,'Hyper Text Margin Language',2,0),
    (5,'Hyper Text Markup Language',2,1),
    (6,'Hyper Textual Markup Language',2,0);
