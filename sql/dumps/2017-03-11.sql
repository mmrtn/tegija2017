-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Märts 11, 2017 kell 08:27 PM
-- Serveri versioon: 5.7.17-0ubuntu0.16.04.1
-- PHP versioon: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `tegija`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `practicalLog`
--

CREATE TABLE `practicalLog` (
  `id` int(11) NOT NULL,
  `answer` mediumtext NOT NULL,
  `points` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `controlledBy` int(11) DEFAULT NULL,
  `questionID` int(11) NOT NULL,
  `studentID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `practicalQuestions`
--

CREATE TABLE `practicalQuestions` (
  `id` int(11) NOT NULL,
  `question` mediumtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adminID` int(11) DEFAULT NULL,
  `studentID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `idCode` varchar(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `testAnswers`
--

CREATE TABLE `testAnswers` (
  `id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `questionID` int(11) NOT NULL,
  `correct` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `testLog`
--

CREATE TABLE `testLog` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questionID` int(11) NOT NULL,
  `answerID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `testQuestions`
--

CREATE TABLE `testQuestions` (
  `id` int(11) NOT NULL,
  `question` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksid tõmmistatud tabelitele
--

--
-- Indeksid tabelile `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indeksid tabelile `practicalLog`
--
ALTER TABLE `practicalLog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_practicalLog_1IDx` (`controlledBy`),
  ADD KEY `fk_practicalLog_2IDx` (`questionID`),
  ADD KEY `fk_practicalLog_3IDx` (`studentID`);

--
-- Indeksid tabelile `practicalQuestions`
--
ALTER TABLE `practicalQuestions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksid tabelile `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token_UNIQUE` (`token`),
  ADD KEY `fk_sessions_1IDx` (`adminID`),
  ADD KEY `fk_sessions_2IDx` (`studentID`);

--
-- Indeksid tabelile `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indeksid tabelile `testAnswers`
--
ALTER TABLE `testAnswers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_testAnswers_1IDx` (`questionID`);

--
-- Indeksid tabelile `testLog`
--
ALTER TABLE `testLog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_testLog_testQuestions1IDx` (`questionID`),
  ADD KEY `fk_testLog_students1IDx` (`studentID`),
  ADD KEY `fk_testLog_1IDx` (`answerID`);

--
-- Indeksid tabelile `testQuestions`
--
ALTER TABLE `testQuestions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT tõmmistatud tabelitele
--

--
-- AUTO_INCREMENT tabelile `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `practicalLog`
--
ALTER TABLE `practicalLog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `practicalQuestions`
--
ALTER TABLE `practicalQuestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `testAnswers`
--
ALTER TABLE `testAnswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `testLog`
--
ALTER TABLE `testLog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabelile `testQuestions`
--
ALTER TABLE `testQuestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tõmmistatud tabelite piirangud
--

--
-- Piirangud tabelile `practicalLog`
--
ALTER TABLE `practicalLog`
  ADD CONSTRAINT `fk_practicalLog_1` FOREIGN KEY (`controlledBy`) REFERENCES `admins` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_practicalLog_2` FOREIGN KEY (`questionID`) REFERENCES `practicalQuestions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_practicalLog_3` FOREIGN KEY (`studentID`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Piirangud tabelile `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `fk_sessions_1` FOREIGN KEY (`adminID`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sessions_2` FOREIGN KEY (`studentID`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Piirangud tabelile `testAnswers`
--
ALTER TABLE `testAnswers`
  ADD CONSTRAINT `fk_testAnswers_1` FOREIGN KEY (`questionID`) REFERENCES `testQuestions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Piirangud tabelile `testLog`
--
ALTER TABLE `testLog`
  ADD CONSTRAINT `fk_testLog_1` FOREIGN KEY (`answerID`) REFERENCES `testAnswers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_testLog_students1` FOREIGN KEY (`studentID`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_testLog_testQuestions1` FOREIGN KEY (`questionID`) REFERENCES `testQuestions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;