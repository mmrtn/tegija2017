import {Component, OnInit, EventEmitter} from '@angular/core';
import {Question} from "../models/question";
import {Mockdata} from "../models/mockdata";
import {Output, Input} from "@angular/core/src/metadata/directives";

@Component({
    selector: 'question-form',
    templateUrl: './questionform.component.html',
    styleUrls: ['./questionform.component.css']
})

export class QuestionformComponent implements OnInit {

    question: Question;
    isEditQuestion: boolean = false;
    cancel = false;

    constructor() {
        this.question = Mockdata.getBlankQuestion();
    }

    ngOnInit() {
    }

    ngDestroy() {
        this.cancel = false;
    }

    onSubmit() {
        if (this.validate(-2) == "ok" && !this.cancel) {
            this.onQuestionSubmit.emit(this.question);
            this.question.question="";
            this.question.answers.map((item) => {return item.answer = ""});
        }
    }

    validate(index = -1) {
        if (index > -1) {
            return (this.question.answers[index].answer != "") ? "ok" : "küsimus " +index.toString();
        }
        if (!this.question.question && index < 0) {
            return "Sisesta küsimus!";
        }
        if (index == -2) {
            if (this.question.answers.find((item) => {return item.answer == ""})) {
                // console.log("Mõni vastus on veel sisestamatta");
                return "Mõni vastus on veel sisestamatta"
            }
            if (!this.question.answers.find((item) => {return item.correct == 1})) {
                // console.log("Õige vastus on valimatta!");
                return "Õige vastus on valimatta!";
            }
        }

        return "ok";
    }

    onRightAnswerChosen(index: number) {
        // zero all correct properties of answers...
        this.question.answers.map((item) => {return item.correct = 0});
        this.question.answers[index].correct = 1;
    }

    isCorrectAnswer(index) {
        return this.question.answers[index].correct === 1;
    }

    cancelEdit() {
        this.cancel = true;
        this.onQuestionEditCancel.emit(true);

    }

    @Output() onQuestionSubmit = new EventEmitter<Question>();
    @Output() onQuestionEditCancel = new EventEmitter<boolean>();

    // For Edit only ...
    @Input()
    set editQuestion(value: Question) {
        let questionCopy: Question = JSON.parse(JSON.stringify(value));
        this.question = questionCopy;
        this.isEditQuestion = true;
    }


}
