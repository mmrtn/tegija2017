import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Question} from "../models/question";
import {Input} from "@angular/core/src/metadata/directives";
import {Answer} from "../models/answer";
import {Mockdata} from "../models/mockdata";

@Component({
    selector: 'quiz-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

    private currentQuestion: Question;


    constructor() {
        // this.currentQuestion = Mockdata.getMockQuestion();
    }



    onAnswerClicked(id: number) {
        this.onAnswerChosen.emit(id);
    }

    ngOnInit() {
    }

    @Output() onAnswerChosen = new EventEmitter<number>();

    @Input()
    set question(value: Question) {
        this.currentQuestion = value
    }


    isCorrectAnswer(id: number) : boolean {
        if (this.currentQuestion.answers) {
            let result = this.currentQuestion.answers.find((item) => {
              return item.answerID == id
            });
            return result.correct == 1;
        }
        return false;
    }
}
