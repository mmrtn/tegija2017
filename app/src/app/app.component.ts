import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app works!';
    static userToken = "";

    static getTestToken() {
        return "a211ad14275b1a241195d19872430a8acefebe5d20b47d2a59fdd97beaf28579"
    }

    static setTestToken() {
        AppComponent.userToken = AppComponent.getTestToken();
    }
}
