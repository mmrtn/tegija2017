import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodetaskComponent } from './codetask.component';

describe('CodetaskComponent', () => {
  let component: CodetaskComponent;
  let fixture: ComponentFixture<CodetaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodetaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodetaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
