import { Component, OnInit } from '@angular/core';
import {CodingTask} from "../models/codingtask";
import {AdminService} from "../services/admin.service";
import {Mockdata} from "../models/mockdata";

@Component({
  selector: 'code-tasks',
  templateUrl: './codetask.component.html',
  styleUrls: ['./codetask.component.css'],
  providers: [AdminService]
})
export class CodetaskComponent implements OnInit {

  newCodingTask: CodingTask = Mockdata.getBlankCodingTask();
  codingTasks: Array<CodingTask>;
  showCreateForm: boolean = false;

  constructor(private adminService: AdminService) {
    this.newCodingTask = Mockdata.getBlankCodingTask();

  }

  ngOnInit() {
    this.getAllTasks();
  }

  onSubmit() {

  }

  getAllTasks() {
    this.adminService.getAllCodingTasks().subscribe(
        data=> this.codingTasks = data.reverse(),
        error=>alert(error),
        // ()=>console.log("All coding tasks: "+ JSON.stringify(this.codingTasks))
    );
  }

  onEdit(id: number) {
    console.log("this TASK : "+this.codingTasks[id]);
    this.adminService.updateCodeQuestion(this.codingTasks[id]).subscribe(
        data=> this.getAllTasks(),
        error=>console.log(error),
    );
  }

  onDelete(id: number) {
    this.adminService.deleteCodeQuestion(id).subscribe(
        data=> this.getAllTasks(),
        error=>alert(error),
    );
  }

  createNewCodingTask() {
    if (this.newCodingTask && this.newCodingTask.question.length > 10) {
      this.adminService.createCodeQuestion(this.newCodingTask).subscribe(
          data=> this.getAllTasks(),
          error=>alert(error),
      );
    }

  }
}
