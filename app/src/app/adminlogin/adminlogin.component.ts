import {Component, OnInit, NgZone} from '@angular/core';
import {AdminService} from "../services/admin.service";
import {AppComponent} from "../app.component";
import {Router} from "@angular/router";
import {StorageHelper} from "../helpers/storage.helper";

declare var gapi: any;
@Component({
    selector: 'admin-login',
    templateUrl: './adminlogin.component.html',
    styleUrls: ['./adminlogin.component.css'],
    providers: [AdminService]
})
export class AdminloginComponent implements OnInit {

    userName: string;
    passWord: string;
    failLogin: boolean = false;
    sent: boolean = false;
    errorMessage = "";

    constructor(private adminService: AdminService, private _router: Router, private zone: NgZone) {}

    ngOnInit() {
    }

    onSubmit() {
        this.onLogin();

    }

    onLogin() {
        if (!this.sent) {
            this.adminService.getToken(this.userName, this.passWord).subscribe(
                data => this.setToken(data),
                error=>this.errorMessage = "Vale kasutjanimi või parool!",
                ()=>this.afterLogin()
            );
        }
        this.sent = true;
    }

    onOauth() {
        if (!this.sent) {
            if (gapi) {
                let that = this;
                gapi.auth.authorize({
                    client_id : "481740604878-kr4ri7fee46j2h2te6aa9liglgogffov.apps.googleusercontent.com",
                    apiKey: 'AIzaSyBE3smFxHU00pTuQnXXXvmgv1t2z02nQdU',
                    immediate:false,
                    include_granted_scopes:false,
                    redirect_uri:"postmessage",
                    response_type:"code",
                    scope:"https://www.googleapis.com/auth/userinfo.email",
                }).then(function (A) {
                    that.adminService.getOauthToken(A.code).subscribe(
                        data => that.setToken(data),
                        error=>that.afterLogin(error),
                        ()=>that.afterLogin()
                    );
                });
            }
        }
        this.sent = true;

    }

    setToken(data: any) {
        this.errorMessage = "";
        AppComponent.userToken = data["token"];
        StorageHelper.saveAdminTokenToLS(AppComponent.userToken);
    }

    afterLogin(error?) {
        if (error && this.sent) {
            console.log("error: " + JSON.stringify(error));
            // this.errorMessage = "Teie Google kontol ei ole administraatori õiguseid!";
            this.errorMessage = error._body;
        }
        else console.log("After login: "+AppComponent.userToken);
        this.sent = false;

        this.zone.run(() => this._router.navigate(['/admin/test']));
    }


}
