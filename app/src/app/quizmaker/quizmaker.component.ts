import {Component, OnInit} from '@angular/core';
import {Question} from "../models/question";
import {AdminService} from "../services/admin.service";

@Component({
    selector: 'quiz-maker',
    templateUrl: './quizmaker.component.html',
    styleUrls: ['./quizmaker.component.css'],
    providers: [AdminService],
})
export class QuizmakerComponent implements OnInit {

    editQuestion = -1;
    showCreateForm = false;
    questions: Array<Question>;
    searchText = "";
    searchMatch: number;

    constructor(private adminService: AdminService) {}


    ngOnInit() {
        this.getAllQuestions();
        this.searchMatch = 0;
    }

    getAllQuestions() {
        this.adminService.getAllQuestions().subscribe(
            data=> this.questions = data.reverse(),
            error=>alert(error),
            // ()=>console.log("OUT TEST QUESTIONS: "+JSON.stringify(this.questions))
        );
    }

    creteQuestion(question: Question) {
        console.log("Create CLICKED!");
        this.adminService.createQuestion(question).subscribe(
            data=> this.createQuestionCallBack(data),
            error=>alert(error)
        );
    }

    createQuestionCallBack(data) {
        console.log(JSON.stringify(data));
        this.getAllQuestions();
    }

    onQustionCreated(question: Question) {
        this.creteQuestion(question);
    }

    onQuestionChanged(question: Question) {
        console.log("changer question: "+JSON.stringify(question));
        this.editQuestion = -1;
        this.adminService.updateQuestion(question).subscribe(
            data=> this.getAllQuestions(),
            error=>alert(error)
        );
    }

    onDelete(questionID : number) {

        this.adminService.deleteQuestion(questionID).subscribe(
            data=> this.getAllQuestions(),
            error=>alert(error),
            ()=>console.log("just deleted question ID: "+questionID)
        );
    }

    onAnswerClicked() {}

    isSearchedQuestion(quesiton: string) : boolean {
        if (this.searchText) {
            if (quesiton.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1) {
                return true;
            }
            return false;
            // return (quesiton.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1) ? true : false;
        }
        return true;
    }

    searchQuiz() {
        if (!this.searchText) this.searchMatch = 0;
        else {
            let arr = this.questions.filter((q)=>q.question.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1);
            this.searchMatch=arr.length;
        }


    }
}
