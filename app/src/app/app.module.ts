import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import {routing} from "./app.router";
import {NotfoundComponent} from "./notfound/notfound.component";
import { QuizComponent } from './quiz/quiz.component';
import { QuestionComponent } from './question/question.component';
import {QuizService} from "./services/quiz.service";
import { ProgressComponent } from './progress/progress.component';
import { QuizmakerComponent } from './quizmaker/quizmaker.component';
import { QuestionformComponent } from './questionform/questionform.component';
import {AdminService} from "./services/admin.service";
import { CodetaskComponent } from './codetask/codetask.component';
import { CodingComponent } from './coding/coding.component';
import { TestComponent } from './test/test.component';
import { ResultsComponent } from './results/results.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import {Ng2SmartTableModule} from "ng2-smart-table/src/ng2-smart-table.module";

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    NotfoundComponent,
    QuizComponent,
    QuestionComponent,
    ProgressComponent,
    QuizmakerComponent,
    QuestionformComponent,
    CodetaskComponent,
    CodingComponent,
    TestComponent,
    ResultsComponent,
    AdminloginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    Ng2SmartTableModule
  ],
  providers: [QuizService, AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
