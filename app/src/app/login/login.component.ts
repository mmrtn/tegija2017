import {Component, OnInit, EventEmitter} from '@angular/core';
import {Student} from "../models/student";
import {Output} from "@angular/core/src/metadata/directives";
declare var validateEstonianIDK: any;

@Component({
    selector: 'student-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    student: Student;
    ik: any;

    constructor() {
        this.student = {firstName: "", lastName: "", idCode: null};
    }

    ngOnInit() {}


    onSubmit() {
        if (!this.isNotValidStudnetForm()) {
            this.onStudentDetailsInserted.emit(this.student);
            // console.log("Student emitted!");
        }
    }

    @Output() onStudentDetailsInserted = new EventEmitter<Student>();

    isNotValidStudnetForm(inputName="") {

        if (this.student.firstName.length < 2 || !(/^[a-zA-Z\s\-]+$/.test(this.student.firstName))) {
            // console.log("eesnimim failib");
            return "firstName";
        }

        if (this.student.lastName.length < 2 || !(/^[a-zA-Z\s\-]+$/.test(this.student.lastName))) {
            // console.log("perenimi failib");
            return "lastName";
        }

        if (this.student.idCode != null && !validateEstonianIDK(this.student.idCode.toString())) {
            // console.log("IK failib: "+this.student.idCode);
            // alert("Sisestatud isikukood ei vasta nõuetele!");
            return "idCode";
        }

        if (this.student.idCode == null) return "idCode"

        return "";
    }


}
