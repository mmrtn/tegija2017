import {Component, OnInit} from '@angular/core';
import {AdminService} from "../services/admin.service";
import {Rankings} from "../models/rankings";
declare var initTable: any;

@Component({
    selector: 'results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.css'],
    providers: [AdminService]
})
export class ResultsComponent implements OnInit {

    rankings: Rankings;

    settings = {
        edit: {
            confirmSave: true
        },
        // mode: "external",
        columns: {
            firstName: {
                title: 'Eesnimi',
                editable: false

            },
            lastName: {
                title: 'Perekonnanimi',
                editable: false

            },
            idCode: {
                title: 'Isikukood',
                editable: false

            },
            test: {
                title: 'Testi tulemus',
                editable: false
            },
            practical: {
                title: 'Praktiline osa',
            },
            total: {
                title: 'Kokku',
                editable: false

            },
            started: {
                title: 'Alustas',
                editable: false

            },
            ended: {
                title: 'Lõpetas',
                editable: false

            },
        }
    };
    private saveEvent: any = null;


    constructor(private adminService: AdminService) {
    }

    ngOnInit() {
        this.getAllRankings();
    }

    onDeleteConfirm(event): void {
        console.log("DELETE: " + event);
    }

    onSaveConfirm(event: Event): void {
        this.saveEvent = event;
        console.log("EDIT: " + event['newData'].practical);

    }

    getAllRankings() {
        this.adminService.getRankings().subscribe(
            data=> this.rankingsReady(data),
            error=>alert(error),
            // ()=>initTable()
        );
    }

    rankingsReady(data) {
        this.rankings = data;
        // setTimeout(() => {
        //     initTable();
        // }, 150);
    }

}
