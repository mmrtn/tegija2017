import {Component, OnInit} from '@angular/core';
import {QuizService} from "../services/quiz.service";
import {CodingTask} from "../models/codingtask";
import {AppComponent} from "../app.component";

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css'],
    providers: [QuizService]
})
export class TestComponent implements OnInit {
    private codingTask: CodingTask;

    constructor(private _quizService: QuizService) {
    }

    ngOnInit() {
        AppComponent.setTestToken();
        this.getCodingTask();
    }

    getCodingTask() {
        this._quizService.getCodeTask().subscribe(
            data=>this.codingTask = data,
            error=>alert(error)
        )
    }

    onCodingTaskSubmit(code: string) {
        console.log("Your submitted code: " + code);
    }

}
