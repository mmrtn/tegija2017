
export class StorageHelper {

    static saveAdminTokenToLS(token: string) {
        if (token) {
            localStorage.setItem("token", token);
        }
    }

    static loadAdminTokenFromLS() : string {
        if (localStorage.getItem("token") !== null && localStorage.getItem("token") !== 'undefined') {
            // var userSettings = JSON.parse(localStorage.getItem(item));
            return localStorage.getItem("token");
        }
        return "";
    }

    static removeAdminToken() : void {
        localStorage.removeItem("token");
    }
}
