import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/map";
import {environment} from "../../environments/environment";
import {AppComponent} from "../app.component";
import {Question} from "../models/question";
import {CodingTask} from "../models/codingtask";

@Injectable()
export class AdminService {

    baseURL: string = environment.apiURL;
    headers: Headers = new Headers();

    constructor(private _http: Http) {
        this.headers.append("content-type", "application/json; charset=UTF-8");
        // this.headers.append('X-token', AppComponent.userToken);
    }

    checkIfTokenAdded() {
        if (!this.headers.get('X-token')) {
            let token = (AppComponent.userToken) ? AppComponent.userToken : "";
            // let token = (AppComponent.userToken) ? AppComponent.userToken : AppComponent.getTestToken();
            console.log('X-token: ' + token);
            this.headers.append('X-token', token);
        }
    }

    getAllQuestions() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/admin/test/quiz", {
            headers: this.headers
        }).map(res => res.json());
    }

    createQuestion(question: Question) {
        this.checkIfTokenAdded();
        return this._http.post(this.baseURL + "/admin/test/quiz",
            this.removeQuesitonID(question), {
                headers: this.headers
            }).map(res => res.json());
    }

    updateQuestion(question: Question) {
        this.checkIfTokenAdded();
        return this._http.put(this.baseURL + "/admin/test/quiz",
            question, {
                headers: this.headers
            }).map(res => res.json());
    }

    deleteQuestion(questionID: number) {
        this.checkIfTokenAdded();
        return this._http.delete(this.baseURL + "/admin/test/quiz/" + questionID.toString(),
            {
                headers: this.headers
            }).map(res => res.json());
    }

    removeQuesitonID(question: Question) {
        let withoutID: any = question;
        delete withoutID.questionID;
        withoutID.answers.map(item => delete item.answerID);
        // console.log("withoutID: " + JSON.stringify(withoutID));
        return withoutID;
    }

    getAllCodingTasks() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/admin/practical/questions", {
            headers: this.headers
        }).map(res => res.json());
    }

    deleteCodeQuestion(questionID: number) {
        this.checkIfTokenAdded();
        return this._http.delete(this.baseURL + "/admin/practical/questions/" + questionID.toString(),
            {
                headers: this.headers
            }).map(res => res.json());
    }

    updateCodeQuestion(codeTask: CodingTask) {
        this.checkIfTokenAdded();
        return this._http.put(this.baseURL + "/admin/practical/questions",
            codeTask, {
                headers: this.headers
            }).map(res => res.json());
    }

    createCodeQuestion(codeTask: CodingTask) {
        this.checkIfTokenAdded();
        return this._http.post(this.baseURL + "/admin/practical/questions",
            codeTask, {
                headers: this.headers
            }).map(res => res.json());
    }

    getRankings() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/admin/ranking", {
            headers: this.headers
        }).map(res => res.json());
    }

    getToken(userName: string, passWord: string) {

        return this._http.post(this.baseURL +"/admin/auth",
            {username: userName, password: passWord}, {
                headers: this.headers
            }).map(res => res.json());
    }

    getOauthToken(code: string) {

        return this._http.post(this.baseURL +"/admin/oauth",
            {code: code}, {
                headers: this.headers
            }).map(res => res.json());
    }

}
