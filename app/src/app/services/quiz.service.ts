import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/map";
import {environment} from "../../environments/environment";
import {AppComponent} from "../app.component";
import {Student} from "../models/student";
import {CodingTask} from "../models/codingtask";

@Injectable()
export class QuizService {

    baseURL: string = environment.apiURL;
    headers: Headers = new Headers();

    constructor(private _http: Http) {
        this.headers.append("content-type", "application/json; charset=UTF-8");
    }

    checkIfTokenAdded() {

        if (!this.headers.get('X-token')) {
            this.headers.append('X-token', AppComponent.userToken);
        }
    }

    getQuiz() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/test", {
            headers: this.headers
        }).map(res => res.json());
    }

    getQuizResults() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/test/results", {
            headers: this.headers
        }).map(res => res.json());
    }

    choseAnswer(answerId: number) {
        this.checkIfTokenAdded();
        return this._http.post(this.baseURL + "/test",
            {answerID: answerId}, {
                headers: this.headers
            }).map(res => res.json());
    }


    getToken(student: Student) {
        return this._http.post(this.baseURL + "/student",
            student, {
                headers: this.headers
            }).map(res => res.json());
    }

    getCodeTask() {
        this.checkIfTokenAdded();
        return this._http.get(this.baseURL + "/practical", {
            headers: this.headers
        }).map(res => res.json());
    }

    sendCodeQuestionAnswer(id: number, codeAnswer: string) {
        this.checkIfTokenAdded();
        return this._http.post(this.baseURL + "/practical",
            {
                questionID: id,
                answer: codeAnswer
            }, {
                headers: this.headers
            }).map(res => res.json());
    }


}
