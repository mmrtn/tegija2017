import {Routes, RouterModule} from "@angular/router";
import {NotfoundComponent} from "./notfound/notfound.component";
import {AdminComponent} from "./admin/admin.component";
import {LoginComponent} from "./login/login.component";
import {QuizComponent} from "./quiz/quiz.component";
import {ProgressComponent} from "./progress/progress.component";
import {QuestionformComponent} from "./questionform/questionform.component";
import {CodingComponent} from "./coding/coding.component";
import {CodetaskComponent} from "./codetask/codetask.component";
import {TestComponent} from "./test/test.component";
import {AdminloginComponent} from "./adminlogin/adminlogin.component";


const APP_ROUTES: Routes = [
  {path: '', redirectTo: '/quiz', pathMatch: 'full'},
  {path: 'admin', component: AdminComponent},
  {path: 'admin/:section', component: AdminComponent},
  {path: 'adminlogin', component: AdminloginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'code', component: TestComponent},
  {path: 'codeingtasks', component: CodetaskComponent},
  {path: 'quiz', component: QuizComponent},
  {path: 'form', component: QuestionformComponent},
  // {path: 'test/:characterId', component: GameWorldComponent},

  {path: '404', component: NotfoundComponent},
  {path: '**', redirectTo: '/quiz'}
];


export const routing = RouterModule.forRoot(APP_ROUTES);
