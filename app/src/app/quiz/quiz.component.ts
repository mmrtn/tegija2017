import {Component, OnInit} from '@angular/core';
import {Question} from "../models/question";
import {QuizService} from "../services/quiz.service";
import {AppComponent} from "../app.component";
import {Student} from "../models/student";
import {CodingTask} from "../models/codingtask";
import {TestResult} from "../models/testresult";
import {Router} from "@angular/router";

@Component({
    selector: 'app-quiz',
    templateUrl: './quiz.component.html',
    styleUrls: ['./quiz.component.css'],
    providers: [QuizService]
})
export class QuizComponent implements OnInit {

    quizLength: number;
    quiz: Array<Question>;
    codingTask: CodingTask;
    isQuizTest: boolean = true;
    isCodingTask: boolean = false;

    testResults: TestResult;

    constructor(private _quizService: QuizService, private _router: Router) {}

    ngOnInit() {
    }

    getQuiz() {
        if (AppComponent.userToken) {
            this._quizService.getQuiz().subscribe(
                data => this.getQuizCallback(data),
                error=>alert(error),
                ()=>console.log(JSON.stringify(this.quiz))
            );
        }
    }

    getQuizCallback(data: any) {
        this.quiz = data;
        this.quizLength = this.quiz.length;
    }

    onStudentLogin(student: Student) {
        this._quizService.getToken(student).subscribe(
            data => AppComponent.userToken = data["token"],
            error=>alert(error),
            ()=>this.getQuiz()
        );
    }

    onAnswerChosenFromChildComp(id: number) {
        this._quizService.choseAnswer(id).subscribe(
            data=>this.removeQuiz(id),
            error=>alert(error)
        )
    }

    getTestResults() {
        this._quizService.getQuizResults().subscribe(
            data=>this.testResults = data[0],
            error=>alert(error),
            ()=>console.log("RESULTS: " + JSON.stringify(this.testResults))
        )
    }

    private removeQuiz(id: number) {
        this.quiz.shift();
        if (this.quiz.length == 0) this.getTestResults();
    }

    getQuizQuestion() {
        if (this.isQuizTest) {
            if (this.quiz && this.quiz.length > 0) {
                return this.quiz[0];
            }

            if (this.quizLength && this.quiz.length == 0) {
                this.isCodingTask = true;
                this.isQuizTest = false;
                this.getCodingTask();
            }
        }
        return null;
    }

    isQuizAndTestOver(): boolean {
        return (!this.isQuizTest && !this.isCodingTask);
    }

    getCodingTask() {
        this._quizService.getCodeTask().subscribe(
            data=>this.codingTask = data,
            error=>alert(error)
        )
    }

    onCodingTaskSubmit(codingAnswer: string) {
        this._quizService.sendCodeQuestionAnswer(this.codingTask.id, codingAnswer).subscribe(
            data=>this.isCodingTask = false,
            error=>alert(error)
        )
    }

    closeAll() {
        this.codingTask = null;
        this.isQuizTest = true;
        this.isCodingTask = false;
        this.quizLength = null;
        this.quiz = null;
        AppComponent.userToken = "";
        this._router.navigate(['/', 'start']);
    }


    hasToken(): boolean {
        if (AppComponent.userToken) {
            return true;
        }
        return false;
    }


}
