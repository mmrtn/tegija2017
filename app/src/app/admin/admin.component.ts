import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AppComponent} from "../app.component";
import {StorageHelper} from "../helpers/storage.helper";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    private section: string;
    private sub: any;

    constructor(private _route: ActivatedRoute, private _router: Router) {

    }

    ngOnInit() {
        this.isSection();
        AppComponent.userToken = StorageHelper.loadAdminTokenFromLS();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    isSection() {
        this.sub = this._route.params.subscribe(params => {
            if (params['section']) {
                console.log(params['section']);
                this.section = params['section'];
            }
            else {
                this._router.navigate(['/admin/', 'test']);
                this.section = 'test';
            }
        });
    }

    isAuth() {
        return (AppComponent.userToken) ? true : false;
    }

    logout() {
        console.log("logout!");
        AppComponent.userToken = "";
        StorageHelper.removeAdminToken();
    }


}
