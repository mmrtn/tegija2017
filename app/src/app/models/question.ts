import {Answer} from "./answer";

export class Question {
  questionID: number;
  question: string;
  answers: Array<Answer>;

  constructor(id: number, question: string, answers: Array<Answer>) {
    this.questionID = id;
    this.question = question;
    this.answers = answers;
  }
}
