import {Answer} from "./answer";
import {Question} from "./question";
import {CodingTask} from "./codingtask";

export class Mockdata {

    static blankAnswers: Array<Answer> = [
        {
            answerID: 0,
            answer: "",
            correct: 0
        },
        {
            answerID: 1,
            answer: "",
            correct: 0
        },
        {
            answerID: 2,
            answer: "",
            correct: 0
        }
    ];

    static getMockQuestion() {
        let answers: Array<Answer> = [
            {
                "answer": "Tallinn",
                "answerID": 1,
                "correct": 1
            },
            {
                "answer": "Tartu",
                "answerID": 2,
                "correct": 0
            },
            {
                "answer": "Pärnu",
                "answerID": 3,
                "correct": 0
            }];
        return new Question(1, "Mis on Eesti pealinn?", answers);
    }

    static getBlankCodingTask() : CodingTask {
        return {
            id: 0,
            question: "",
            imgUrl: ""
        }
    }

    static getBlankQuestion(): Question {
        return new Question(0, "", Mockdata.blankAnswers);
    }

}
