export interface Answer {
    answerID: number
    answer: string
    correct: number
}
