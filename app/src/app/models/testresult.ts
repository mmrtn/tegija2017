export interface TestResult {
    correctCount: number,
    answersCount: number
}

