export interface CodingTask {
    id: number
    question: string
    imgUrl: string
}
