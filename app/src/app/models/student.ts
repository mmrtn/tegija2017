export interface Student {
    firstName: string
    lastName: string
    idCode: number
}
