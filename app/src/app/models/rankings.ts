export interface Rankings {
    firstName: string,
    lastName: string,
    idCode: number,
    test: number,
    practical: number,
    total: number,
    started: string,
    ended: string
}

