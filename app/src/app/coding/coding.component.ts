import {Component, OnInit, EventEmitter} from '@angular/core';
import {Output, Input} from "@angular/core/src/metadata/directives";
import {CodingTask} from "../models/codingtask";

@Component({
    selector: 'coding-task',
    templateUrl: './coding.component.html',
    styleUrls: ['./coding.component.css']
})
export class CodingComponent implements OnInit {

    code: string;
    codingTask: CodingTask;
    question: string;
    preview: any;
    showImg: boolean = true;

    constructor() {
    }

    ngOnInit() {
        this.preview = document.getElementById("preview");
        let body: any = document.getElementsByTagName('body')[0];
        body.classList.remove("page-bg");   //add the class
    }

    ngSubmit() {
    }

    @Output() onTaskReady = new EventEmitter<string>();

    @Input()
    set setQuestion(value: CodingTask) {
        this.codingTask = value;
    }

    sentCodeToPreview(value) {
        // console.log("clog: " + value);
        this.preview.innerHTML = value;
    }

    submitCode() {
        // console.log("You submitted followind code:");
        // console.log(this.code);
        this.onTaskReady.emit(this.code);
        this.code="";
        this.sentCodeToPreview(this.code);
    }

    getShowImgTxt() {
        return (this.showImg) ? "Peida pilt" : "Ava pilt";
    }

}
