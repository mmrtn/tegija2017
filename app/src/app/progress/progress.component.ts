import {Component, OnInit} from '@angular/core';
import {Input} from "@angular/core/src/metadata/directives";

@Component({
    selector: 'quiz-progress',
    templateUrl: './progress.component.html',
    styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

    questionsNumber: number;
    questionsLeft: number;
    percent = this.getPercent();

    constructor() {

    }

    ngOnInit() {
        // this.questionsNumber = 5;
        // this.questionsLeft = 2;
    }

    @Input()
    set setQuestionsLeft(value: number) {
        this.questionsLeft = value
        console.log("getPercent: " + this.getPercent());
    }

    @Input()
    set setQustionsNumber(value: number) {
        this.questionsNumber = value
    }

    getCurrentQuestionNr(): number {
        if (this.questionsNumber != this.questionsLeft) {
            let currentQuestionNR = 1 + this.questionsNumber - this.questionsLeft;
            return (currentQuestionNR > this.questionsNumber) ? currentQuestionNR - 1 : currentQuestionNR;
        }

        return 1;
    }

    getPercent() {

        return this.getCurrentQuestionNr() / this.questionsNumber * 100;
    }

    getStrPercent() {
        return (this.getCurrentQuestionNr() / this.questionsNumber * 100).toString() + '%';
    }

}
