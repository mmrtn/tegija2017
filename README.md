### /student ####
*    POST
*     input = 
      {
          "firstName" : "Aadu",
          "lastName" : "Tamm",
          "idCode" : 39607132896
      }
      
*     output = 
        API token



### /test ####
*    GET
* correct: 0 is for simplyfing front-end data structure

*     output = 
          [
              {
                "question": "Eremiitpõrnikat ohustavad kõige rohkem",
                "questionID": 46,
                "answers": [
                  {
                    "answer": "ta enda eluviisid",
                    "answerID": 74,
                    "correct": 0,
                  },
                  {
                    "answer": "vanade tammikute raie ja “korda tegemine”",
                    "answerID": 75,
                    "correct": 0,
                  },
                  {
                    "answer": "muutuv kliima",
                    "answerID": 76,
                    "correct": 0,
                  }
                ]
              }
          ]
*    POST
*     input = 
      {
          "answerID" : 71
      }
      
*     output = {"message": "Created"}
      
### /test/results ###
*    GET
*     output = 
       {
         "correctCount": 2,
         "answersCount": 9
       }
### /practical  ####
*    GET
*     output = 
        [
          {
            "question": ""HTML dokumendi struktuur
            Lisa lehele punane paragrahv 'Olen avatud uutele väljakutsetele!'
            Lisa eelnevalt loodud paragrahvile tiitel 'Väljakutsed, siit ma tulen!'
            Lisa lehele pilt suurusega 250x250px aadressilt:  
            https://s-media-cache-ak0.pinimg.com/originals/1c/bb/54/1cbb542b423fafe951469bcf8651f28c.jpg
            Joonda pilt paremale
            Muuda pilt lingiks, mis suunab lehele: 
            http://khk.ee/
            Lisa kaldkirjas tekst: Eesnimi, Perekonnanimi, Kuupäev""
          }
        ]
*    POST
*     input =
       {
       "questionID" : 4,
       "answer": "Badass"
      }
*    output = 5
        
### /admin ###
   * ### /admin/ranking ###
   * GET
   *      output =
           [
             {
               "firstName": "Aadu",
               "lastName": "Mänd",
               "idCode": "5550215442013",
               "test": "0",
               "practical": 0,
               "total": 0,
               "lastPracticalAnswerID" : 5
             },
             {
               "firstName": "Kadri",
               "lastName": "Kaunis",
               "idCode": "489051174236",
               "test": "9",
               "practical": 0,
               "total": "9",
              "lastPracticalAnswerID" : 0
             },
           ]
   

* ### /admin/practical/questions ###
 * GET
  *      output = 
            [
              {
                "question": "Praktiline küsimus",
                "id": 1
              }
            ]

   * POST
    *      input =   
            {
              "question": "Hello world"
            }
     *       output = 8

     * PUT
     *      input =   
            {
              "question": "Hello world",
              "id" : 8
            }
     *       output = 
             {
                "message":"OK"
             }
     * delete  argument {id}
     *       output = 
             {
                "message":"OK"
             }
             
* ### /admin/practical/answers ###

 * GET argument {question_id}
 *       oputput =
            [
             { 
              "id" : 4,
              "points" : 0,
              "answer" : "<img src='pilt.jpg'>"
            }
           ]
             
    
   * PUT
   *       input = 
           {
             "id" : 4,
             "points" : 5
           }

   *       output = 
           {
              "message":"OK"
           }
 * ### /admin/practical/answers/view/ ###

 * GET argument {answerID}
 * output = html leht, mis sisaldab õpilase nime ja lahendust. Lehe tiitliks on õpilase nimi.
  * ### /admin/test/quiz ###
   * GET
   *     output = 
               {
                 "question": "Eremiitpõrnikat ohustavad kõige rohkem",
                 "questionID": 37,
                 "answers": [
                   {
                     "answer": "ta enda eluviisid",
                     "answerID": 47,
                     "correct": 0
                   },
                   {
                     "answer": "vanade tammikute raie ja “korda tegemine”",
                     "answerID": 48,
                     "correct": 1
                   },
                   {
                     "answer": "muutuv kliima",
                     "answerID": 49,
                     "correct": 0
                   }
                 ]
               } 

   * POST
   *     input = 
            {
      
            "question": "Eremiitpõrnikat ohustavad kõige rohkem",
            "answers": [
                  {
                      "answer": "ta enda eluviisid",
                      "correct": 0
                  },
                  {
                      "answer": "vanade tammikute raie ja “korda tegemine",
                      "correct": 1
                  },
                  {
                      "answer": "muutuv kliima",
                      "correct": 0
                  }
              ]
            }
   *     output =
            {
              "questionID": "72",
              "answers": [
                "92",
                "93",
                "94"
              ]
            }
      
   *  PUT
   *     input =
            {
            "question": "Eremiitpõrnikat ohustavad kõige rohkem",
            "questionID" : 42,
            "answers": [
                 {
                     "answer": "ta enda eluviisid",
                     "correct": 0,
                     "answerID" : 23
                 },
                 {
                     "answer": "vanade tammikute raie ja “korda tegemine”",
                     "correct": 1,
                     "answerID" : 24
                 },
                 {
                     "answer": "muutuv kliima",
                     "correct": 0,
                     "answerID" : 25
                 }
             ]
            }
   *     output =
            {
                "message": "OK"
            }
  *  DELETE argument {id}
   *     output = {
                    "message": "OK"
                  }
    
     ### /admin/test/results ###
 *  GET
*      output = 
        {
          "studentID": 1,
          "correctCount": "0",
          "answersCount": "3"
        }
        
        
     ### /admin/admins ###
 *  GET
*      output = 
        [
          {
            "email": "admin@gmail.com",
            "firstName": "Admin",
            "lastName": "Kasutaja"
          }
        ]
*    POST
*       input = 
         {
            "email": "admin@admin.com"
         }
*        output =
         {
            "message":"Created"
         }
        CODE: 201
        
 * DELETE  argument {id}
 *       output = 
         {
            "message":"OK"
         }
